package com.keycloak.demo.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class AppController {

    @GetMapping("/user")
    public Mono<String> user(@AuthenticationPrincipal Jwt jwt) {
        return Mono.just(String.format("Hello, %s!", jwt.getClaimAsString("name")));
    }

}
